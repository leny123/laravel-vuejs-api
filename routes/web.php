<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', function() {
    return view('layouts.app');
});

Route::get('/obchod', [ProductsController::class, 'showProductsListing'])->name('showProductsListing');
Route::get('/obchod/{category}', [ProductsController::class, 'showCategory'])->name('products.category');
Route::get('/obchod/{category}/{subcategory}', [ProductsController::class, 'showSubcategory'])->name('products.subcategory');

Route::post('/cart/add', [CartController::class, 'store'])->name('cart.store');
Route::post('/cart/remove', [CartController::class, 'destroy'])->name('cart.destroy');
Route::post('/cart/update', [CartController::class, 'update'])->name('cart.update');
Route::post('/cart/getCartSession', [CartController::class, 'getCartSession']);
Route::post('/cart/getCheckoutSession', [CartController::class, 'getCheckoutSession']);
Route::post('/cart/getTotalSession', [CartController::class, 'getTotalSession']);
Route::post('/cart/checkout/add', [CartController::class, 'storeCheckout'])->name('cart.checkout.store');
Route::post('/cart/sendOrder', [CartController::class, 'sendOrder'])->name('cart.sendOrder');

Route::post('/product/filter', [ProductsController::class, 'filterProducts']);
Route::post('/product/search', [ProductsController::class, 'searchProducts']);
Route::post('/rating/canDestroy', [RatingsController::class, 'canDestroy']);

Route::any('{slug}', function() {
    return view('layouts.app');
});
Route::any('{slug}/{slug2}', function() {
    return view('layouts.app');
});
Route::any('{slug}/{slug2}/{slug3}', function() {
    return view('layouts.app');
});
Route::any('{slug}/{slug2}/{slug3}/{slug4}', function() {
    return view('layouts.app');
});