<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/featured/categories', [CategoriesController::class, 'showFeatured']);
Route::get('/featured/products', [ProductsController::class, 'showFeatured']);
Route::get('/permissions', [HomeController::class, 'checkPermission']);

//products
Route::apiResource('/products', ProductsController::class);
Route::apiResource('products.attributes', AttributeProductController::class);
Route::apiResource('products.values', ValueProductController::class);
Route::apiResource('products.ratings', RatingsController::class);
Route::get('/cart/products', [CartController::class, 'showProducts']);
Route::post('/products/storeImage', [ProductsController::class, 'storeProductImage']);

//categories

Route::apiResource('/categories', CategoriesController::class);
Route::apiResource('categories.products', ProductCategoryController::class);
Route::apiResource('categories.subcategories', CategorySubcategoryController::class);
Route::post('/categories/storeImage', [CategoriesController::class, 'storeCategoryImage']);
Route::post('/categories/updateFeatured', [CategoriesController::class, 'updateFeatured']);

//subcategories
Route::apiResource('/subcategories', SubcategoriesController::class);
Route::apiResource('subcategories.products', ProductSubcategoryController::class);

//attributes
Route::apiResource('/attributes', AttributesController::class);
Route::apiResource('/admin/values', Admin\ValuesController::class);

//orders
Route::apiResource('/orders', OrdersController::class);

