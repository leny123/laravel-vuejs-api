/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue').default;
import VueSweetalert2 from 'vue-sweetalert2';
import ViewUI from 'view-design';
import Chartkick from 'vue-chartkick'
import Chart from 'chart.js'
import router from './router'
import VModal from 'vue-js-modal'


Vue.use(ViewUI);
Vue.use(Chartkick.use(Chart));
Vue.use(ViewUI);
Vue.use(VModal);
Vue.use(VueSweetalert2);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))


Vue.component('main-app', require('./components/MainApp.vue').default);
Vue.component('home-component', require('./components/HomeComponent.vue').default);
Vue.component('order-success-component', require('./components/cart/OrderSuccessComponent.vue').default);

//products
Vue.component('products-component', require('./components/ProductsComponent.vue').default);
Vue.component('product-component', require('./components/ProductComponent.vue').default);
Vue.component('index-product', require('./components/admin/products/IndexProduct.vue').default);
Vue.component('create-product-component', require('./components/admin/products/createProductComponent.vue').default);
Vue.component('edit-product-component', require('./components/admin/products/editProductComponent.vue').default);
Vue.component('product-search-component', require('./components/ProductSearchComponent.vue').default);
Vue.component('product-chart-component', require('./components/ProductChartComponent.vue').default);

//orders
Vue.component('index-order', require('./components/admin/orders/IndexOrder.vue').default);

//cart
Vue.component('cart-total-component', require('./components/cart/CartTotalComponent.vue').default);
Vue.component('cart-count-component', require('./components/cart/CartCountComponent.vue').default);
Vue.component('cart-component', require('./components/cart/CartComponent.vue').default);
Vue.component('checkout-component', require('./components/cart/CheckoutComponent.vue').default);
Vue.component('cart-review-component', require('./components/cart/CartReviewComponent.vue').default);

Vue.component('kategorie-menu-component', require('./components/KategorieMenuComponent.vue').default);

//admin
Vue.component('index-admin', require('./components/admin/IndexAdmin.vue').default);

//attributes
Vue.component('index-attribute', require('./components/admin/attributes/IndexAttribute.vue').default);
Vue.component('create-attribute-component', require('./components/admin/attributes/CreateAttributeComponent.vue').default);
Vue.component('edit-attribute-component', require('./components/admin/attributes/editAttributeComponent.vue').default);

//categories
Vue.component('categories-list', require('./components/CategoriesList.vue').default);
Vue.component('index-category', require('./components/admin/categories/IndexCategory.vue').default);
Vue.component('create-category-component', require('./components/admin/categories/createCategoryComponent.vue').default);
Vue.component('edit-category-component', require('./components/admin/categories/editCategoryComponent.vue').default);

//subcategories
Vue.component('create-subcategory-component', require('./components/admin/subcategories/createSubcategoryComponent.vue').default);
Vue.component('edit-subcategory-component', require('./components/admin/subcategories/editSubcategoryComponent.vue').default);

//values
Vue.component('create-value-component', require('./components/admin/values/createValueComponent.vue').default);
Vue.component('edit-value-component', require('./components/admin/values/editValueComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
    const app = new Vue({
        el: '#app',
        router
    });
