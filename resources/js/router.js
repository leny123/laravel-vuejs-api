import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

import home from './components/HomeComponent'
import orderSuccess from './components/cart/OrderSuccessComponent'
import productChart from './components/ProductChartComponent'

import product from './components/ProductComponent'
import indexProduct from './components/admin/products/IndexProduct'
import createProduct from './components/admin/products/createProductComponent'
import editProduct from './components/admin/products/editProductComponent'

import cartComponent from './components/cart/CartComponent'
import checkoutComponent from './components/cart/CheckoutComponent'
import cartReviewComponent from './components/cart/CartReviewComponent'

import indexAdmin from './components/admin/IndexAdmin'

import indexAttribute from './components/admin/attributes/IndexAttribute'
import createAttribute from './components/admin/attributes/CreateAttributeComponent'
import editAttribute from './components/admin/attributes/EditAttributeComponent'

import createValue from './components/admin/values/CreateValueComponent'
import editValue from './components/admin/values/EditValueComponent'

import categoriesList from './components/CategoriesList'
import indexCategory from './components/admin/categories/IndexCategory'
import createCategory from './components/admin/categories/CreateCategoryComponent'
import editCategory from './components/admin/categories/EditCategoryComponent'

import createSubcategory from './components/admin/subcategories/CreateSubcategoryComponent'
import editSubcategory from './components/admin/subcategories/EditSubcategoryComponent'

import indexOrder from './components/admin/orders/IndexOrder'


const routes = [

    {
        path: '/kategorie',
        component: categoriesList,
    },
    {
        path: '/',
        component: home
    },
    {
        path: '/cart',
        component: cartComponent
    },
    {
        path: '/cart/checkout',
        component: checkoutComponent
    },
    {
        path: '/cart/review',
        component: cartReviewComponent
    },
    {
        path: '/cart/success',
        component: orderSuccess
    },

    //product 
    {
        path: '/product/:slug',
        component: product
    },
    {
        path: '/admin/products',
        component: indexProduct
    },
    {
        path: '/admin/new/product',
        component: createProduct
    },
    {
        path: '/admin/edit/product/:slug',
        component: editProduct
    },
    {
        path: '/product/chart/:slug',
        component: productChart
    },

    //admin
    {
        path: '/admin',
        component: indexAdmin
    },
        //attributes
        {
            path: '/admin/attributes',
            component: indexAttribute
        },
        {
            path: '/admin/new/attribute',
            component: createAttribute
        },
        {
            path: '/admin/attributes/edit/:slug',
            component: editAttribute
        },

        //values
        {
            path: '/admin/new/value',
            component: createValue
        },
        {
            path: '/admin/edit/value/:slug',
            component: editValue
        },

        //categories
        {
            path: '/admin/categories',
            component: indexCategory
        },
        {
            path: '/admin/new/category',
            component: createCategory
        },
        {
            path: '/admin/edit/category/:slug',
            component: editCategory
        },

        //subcategories
        {
            path: '/admin/new/subcategory',
            component: createSubcategory
        },
        {
            path: '/admin/edit/subcategory/:slug',
            component: editSubcategory
        },
        //orders
        {
            path: '/admin/orders',
            component: indexOrder
        },
]

export default new Router({
    mode: 'history',
    routes
})