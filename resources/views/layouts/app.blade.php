<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="//unpkg.com/view-design/dist/styles/iview.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" integrity="sha512-vKMx8UnXk60zUwyUnUPM3HbQo8QfmNx7+ltw8Pm5zLusl1XIfwcxo8DbWCqMGKaWeNxWA8yrx5v3SaVpMvR3CA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script>
        var Turbolinks = require("turbolinks")
        Turbolinks.start()
    </script>

</head>
<body>
    <div id="app" v-cloak>
        <nav class="navbar navbar-expand-lg navbar-light bg-white shadow-sm d-lg-none" style="z-index: 9999;">
            <div class="container">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" style="width: 400px;" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <div class="d-flex">
                        <div class="d-flex mx-auto">
                            @guest
                                @if (Route::has('login'))
                                    <a class="nav-link my-auto text-secondary" href="{{ route('login') }}"><i class="fas fa-user h2 my-auto"></i></a>
                                @endif
                            @else
                                <h5 class="m-1 text-success">{{ Auth::user()->name }}</h5>
                                @if(Auth::user()->hasRole('Admin'))
                                    <a class="dropdown-item" href="{{ url('/admin') }}">
                                        Admin menu
                                    </a>
                                @endif
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                    Odhlásit se
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            @endguest
                        </div>
                    </div>
                    
                    <div class="d-flex">
                        <div class="mx-auto d-flex">
                            <a href="{{ url('/obchod') }}" class="nav-link my-auto">
                                <h5 class="text-dark text-bold text-center">Obchod</h5>
                            </a>
                            <a href="{{ url('kategorie') }}" class="nav-link my-auto">
                                <h5 class="text-dark text-bold text-center">Všechny kategorie</h5>
                            </a>
                            <a class="nav-link my-auto d-flex" href="{{ url('/cart') }}">
                                <cart-count-component :cart-session="{{ json_encode(Session::get('cart')) }}"></cart-count-component>
                            </a>
                        </div>
                    </div>
                    <ul class="navbar-nav mx-auto">
                        <li class="nav-item d-flex mx-auto">
                            <product-search-component></product-search-component>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <nav class="navbar navbar-expand-lg navbar-light bg-white shadow-sm d-none d-lg-block">
            <div class="container">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        <a class="navbar-brand" href="{{ url('/') }}">
                            <img src="{{ asset('images/Logo.svg') }}">
                        </a>
                        <li class="nav-item d-flex mx-2">
                            <a href="{{ url('/obchod') }}" class="nav-link my-auto">
                                <h5 class="text-dark text-bold text-center">Obchod</h5>
                            </a>
                        </li>
                        <li class="nav-item d-flex mx-2">
                            <a href="{{ url('kategorie') }}" class="nav-link my-auto">
                                <h5 class="text-dark text-bold text-center">Všechny kategorie</h5>
                            </a>
                        </li>
                    </ul>
                    <ul class="navbar-nav mx-auto">
                        <li class="nav-item d-flex mx-5">
                            <product-search-component></product-search-component>
                        </li>
                    </ul>
                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        

                        <li class="nav-item d-flex">
                            <a class="nav-link my-auto d-flex" href="{{ url('/cart') }}">
                                <cart-count-component :cart-session="{{ json_encode(Session::get('cart')) }}"></cart-count-component>
                            </a>
                        </li>
                        <!-- Authentication Links -->
                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item d-flex">
                                    <a class="nav-link my-auto" href="{{ route('login') }}"><i class="fas fa-user h2 my-auto"></i></a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown d-flex my-auto">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    @if(Auth::user()->hasRole('Admin'))
                                        <a class="dropdown-item" href="{{ url('/admin') }}">
                                            Admin menu
                                        </a>
                                    @endif

                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Odhlásit se
                                    </a>
                                    
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
            <main-app></main-app>
        </main>

        <footer class="bg-dark text-center text-white">
            <div class="d-flex py-5">
                <div class="col">
                    <h3>Naše sociální sítě:</h3>
                    Instagram
                </div>
                <div class="col">
                    <h3>Naše sociální sítě:</h3>
                    Instagram
                </div>
                <div class="col">
                    <h3>Naše sociální sítě:</h3>
                    Instagram
                </div>
            </div>
        </footer>
    </div>
</body>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js" integrity="sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" integrity="sha512-RXf+QSDCUQs5uwRKaDoXt55jygZZm2V++WUZduaU/Ui/9EGp3f/2KZVahFZBKGH0s774sd3HmrhUy+SgOFQLVQ==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>


</html>
