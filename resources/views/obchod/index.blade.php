@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-3">
            <div class="row">
                <kategorie-menu-component route-url={{ $routeurl }} subcategory-url={{ $subcategory_slug}}></kategorie-menu-component>
            </div>
        </div>
        <div class="col-9">
            <products-component route-url={{ $routeurl }} category={{ $category }} subcategory-url={{ $subcategory }}></products-component>
        </div>
    </div>
</div>
@endsection
