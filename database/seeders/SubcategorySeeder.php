<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Subcategory;

class SubcategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Subcategory::truncate();

        $tricka = Subcategory::create([
            'display_name' => 'Trička',
            'slug' => 'tricka'
        ]);
        $telefony = Subcategory::create([
            'display_name' => 'Telefony',
            'slug' => 'telefony'
        ]);
        $tenisky = Subcategory::create([
            'display_name' => 'Tenisky',
            'slug' => 'tenisky'
        ]);
        $horror = Subcategory::create([
            'display_name' => 'Horror',
            'slug' => 'horror'
        ]);
        $zahrada = Subcategory::create([
            'display_name' => 'Zahrada',
            'slug' => 'zahrada'
        ]);

        $tricka->categories()->sync(1);
        $tenisky->categories()->sync(2);
        $telefony->categories()->sync(3);
        $horror->categories()->sync(4);
        $zahrada->categories()->sync(5);
        
    }
}
