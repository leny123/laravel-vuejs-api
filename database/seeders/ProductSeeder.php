<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Product;
use App\Models\Category;
use App\Models\Attribute;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::truncate();
        $categories = Category::all();
        $attributes = Attribute::all();
        foreach($categories as $category) {
            $category->products()->detach();
        }
        foreach($attributes as $attribute) {
            $attribute->products()->detach();
        }
        for ($i = 0; $i < 100; $i++) {
            $randomCategory = rand(0,4);
            if($randomCategory == 0) {
                $products = ['Mikina', 'Tričko', 'Džíny'];
            } elseif($randomCategory == 1) {
                $products = ['Nike', 'Adidas', 'Jordan'];
            } elseif($randomCategory == 2) {
                $products = ['Počítač', 'Sluchátka', 'Notebook'];
            } elseif($randomCategory == 3) {
                $products = ['Harry Potter', 'Piráti z karibiku', 'Bible'];
            } else {
                $products = ['Hrábě', 'Hlína', 'Kolečka'];
            }
            $product = Product::create([
                'name' => $products[rand(0,2)],
                'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Adipisci, tenetur. Nam, odio amet. Ratione eos quos dignissimos nobis harum, ipsum consectetur quod qui sint tempore, doloremque inventore praesentium laboriosam placeat.',
                'image' => 'product-image-placeholder.png',
                'price' => rand(499,999),
                'last_price' => 0
            ]);
            $product->categories()->attach($categories[$randomCategory]);
            $product->subcategories()->sync($categories[$randomCategory]->subcategories()->first());
            $x = 0;
            while ($x < 3) {
                $attribute = $attributes[$x];
                $product->attributes()->attach($attribute);
                $product->values()->attach([
                    $attribute->values[$x]->id => [
                        'stock' => rand(1,500)
                    ]
                ]);
                $x++;
            }
        }
    }
}
