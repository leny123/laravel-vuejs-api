<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\User;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        User::truncate();
        DB::table('role_user')->truncate();

        $managerRole = Role::where('name', 'manager')->first();
        $adminRole = Role::where('name', 'admin')->first();
        $userRole = Role::where('name', 'user')->first();

        $adminUser = User::create([
            'name' => 'Admin',
            'email' => 'admin@admin.cz',
            'password' => Hash::make('123')
        ]);
        $managerUser = User::create([
            'name' => 'Manager',
            'email' => 'manager@manager.cz',
            'password' => Hash::make('123')
        ]);
        $normalUser = User::create([
            'name' => 'User',
            'email' => 'user@user.cz',
            'password' => Hash::make('123')
        ]);

        $adminUser->roles()->attach($adminRole);
        $managerUser->roles()->attach($managerRole);
        $normalUser->roles()->attach($userRole);
        
    }
}
