<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Attribute;

class AttributeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Attribute::truncate();

        Attribute::create([
            'display_name' => 'Velikost',
            'slug' => 'velikost'
        ]);

        Attribute::create([
            'display_name' => 'Příchuť',
            'slug' => 'prichut'
        ]);

        Attribute::create([
            'display_name' => 'Barva',
            'slug' => 'barva'
        ]);
    }
}
