<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Subattribute;

class SubattributeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Subattribute::truncate();

        Subattribute::create([
            'display_name' => 'Barva',
            'slug' => 'barva'
        ]);

    }
}
