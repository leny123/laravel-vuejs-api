<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Value;
use App\Models\Attribute;
use App\Models\Subattribute;

class ValueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Value::truncate();
        $attributes = Attribute::all();
        foreach($attributes as $attribute) {
            $attribute->values()->detach();
        }
        $attributeVelikost = Attribute::where('slug', 'velikost')->first();
        $attributePrichut = Attribute::where('slug', 'prichut')->first();
        $attributeBarva = Attribute::where('slug', 'barva')->first();
        $modra = Value::create([
            'display_value' => 'Modrá',
            'slug' => 'modra'
        ]);

        $zelena = Value::create([
            'display_value' => 'Zelená',
            'slug' => 'zelena'
        ]);
        $cervena = Value::create([
            'display_value' => 'Červená',
            'slug' => 'cervena'
        ]);

        $velikost1 = Value::create([
            'display_value' => '39',
            'slug' => '39'
        ]);
        $velikost2 = Value::create([
            'display_value' => '40',
            'slug' => '40'
        ]);
        $velikost3 = Value::create([
            'display_value' => '41',
            'slug' => '41'
        ]);
        

        $prichut1 = Value::create([
            'display_value' => 'Vanilková',
            'slug' => 'vanilkova'
        ]);
        $prichut2 = Value::create([
            'display_value' => 'Pomerančová',
            'slug' => 'pomerancova'
        ]);
        $prichut3 = Value::create([
            'display_value' => 'Melounová',
            'slug' => 'melounova'
        ]);

        $attributeBarva->values()->attach($modra);
        $attributeBarva->values()->attach($zelena);
        $attributeBarva->values()->attach($cervena);

        $attributeVelikost->values()->attach($velikost1);
        $attributeVelikost->values()->attach($velikost2);
        $attributeVelikost->values()->attach($velikost3);
        
        $attributePrichut->values()->attach($prichut1);
        $attributePrichut->values()->attach($prichut2);
        $attributePrichut->values()->attach($prichut3);
    }
}
