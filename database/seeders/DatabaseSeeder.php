<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(SubcategorySeeder::class);
        $this->call(AttributeSeeder::class);
        $this->call(SubattributeSeeder::class);
        $this->call(ValueSeeder::class);
        $this->call(ProductSeeder::class);
    }
}
