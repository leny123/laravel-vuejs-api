<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::truncate();
        
        Category::create([
            'display_name' => 'Oblečení',
            'slug' => 'obleceni',
            'image' => 'kategorie_obleceni.png',
            'featured' => 1
        ]);
        Category::create([
            'display_name' => 'Sport',
            'slug' => 'sport',
            'image' => 'kategorie_sport.png',
            'featured' => 1
        ]);
        Category::create([
            'display_name' => 'Elektronika',
            'slug' => 'elektronika',
            'image' => 'kategorie_elektronika.png',
            'featured' => 1
        ]);
        Category::create([
            'display_name' => 'Knihy',
            'slug' => 'knihy',
            'image' => 'kategorie_knihy.png',
            'featured' => 1
        ]);
        Category::create([
            'display_name' => 'Venkovní potřeby',
            'slug' => 'venkovni_potreby',
            'image' => 'kategorie_venkovni_potreby.png',
            'featured' => 1
        ]);
    }
}
