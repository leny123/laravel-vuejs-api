<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\AttributesResource;
use App\Models\Product;
use App\Models\Attribute;

class AttributeProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Product $product)
    {
        return AttributesResource::collection($product->attributes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Product $product)
    {
        if(Auth::check()) {
            if(!$request->user()->hasRole('admin')) {
                return response(['error' => 'Nemáte dostatečná oprávnění']);
            }
        } else {
            return response(['error' => 'Nemáte dostatečná oprávnění']);
        }
        $product->attributes()->create([
            'display_name' => $request->display_name,
            'slug' => $request->slug
        ]);
        return response(['success' => 'Možnost úspěšně vytvořena']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product, Attribute $attribute)
    {
        foreach($product->attributes as $productAttribute) {
            if($productAttribute->pivot->attribute_id == $attribute->id) {
                return new AttributesResource($attribute);
            } 
        }
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product, Attribute $attribute)
    {
        if(Auth::check()) {
            if(!$request->user()->hasRole('admin')) {
                return response(['error' => 'Nemáte dostatečná oprávnění']);
            }
        } else {
            return response(['error' => 'Nemáte dostatečná oprávnění']);
        }
        foreach($product->attributes as $productAttribute) {
            if($productAttribute->pivot->attribute_id == $attribute->id) {
                $attribute->display_name = $request->display_name;
                $attribute->slug = $request->slug;
                $attribute->save();
                return response(['success' => 'Možnost úspěšně aktualizována.']);
            } 
        }
        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Product $product, Attribute $attribute)
    {
        if(Auth::check()) {
            if(!$request->user()->hasRole('admin')) {
                return response(['error' => 'Nemáte dostatečná oprávnění']);
            }
        } else {
            return response(['error' => 'Nemáte dostatečná oprávnění']);
        }
        foreach($product->attributes as $productAttribute) {
            if($productAttribute->pivot->attribute_id == $attribute->id) {
                $attribute->products()->detach();
                $attribute->delete();

                return response(['success' => 'Možnost úspěšně odebrána']);
            } 
        }
        abort(404);
    }
}
