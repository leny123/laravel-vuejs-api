<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Resources\ProductsResource;
use App\Http\Requests\CheckoutRequest;
use App\Services\CartService;
use App\Models\Product;
use App\Models\Value;

class CartController extends Controller
{
    public function store(Request $request) {
        $values = Value::find($request->values);
        $product = Product::find($request->product);
        if(!empty(Session::get('cart'))) {
            if(Session::get('cart.' . $request->product)) {
                return response(['warning' => 'Tenhle produkt již máte v košíku']);
            }
        }
        if($request->values) {
            if((new CartService())->checkStock($request, $product) == 0){
                return response(['error' => 'Tenhle produkt je momentálně vyprodaný']);
            } elseif((new CartService())->checkStock($request, $product) == 1) {
                return response(['error' => 'Snažíte se objednat více kusů než máme na skladě']);
            }
        }
        ($request->values) 
         ? $cartItem = (new CartService())->addToCartWithValues($request, $values) 
         : $cartItem = (new CartService())->addToCartDefault($product); 
        Session::push('cart.' . $request->product, $cartItem);

        $products = Product::whereIn('id', array_keys(Session::get('cart')))->get();
        $total = (new CartService())->getCartTotal(Session::get('cart'), $products, Session::get('checkout'));
        Session::put('total', $total);

        return response(['success' => 'Produkt přidán do košíku']);
    }

    public function storeCheckout(CheckoutRequest $request) {
        $request->validated();
        $checkoutArray = (new CartService())->addToCheckout($request); 
        Session::put('checkout', $checkoutArray);
        
        $products = Product::whereIn('id', array_keys(Session::get('cart')))->get();
        $total = (new CartService())->getCartTotal(Session::get('cart'), $products, Session::get('checkout'));
        Session::put('total', $total);
        
        return response(['message' => 'Produkt přidán do košíku']);
    }

    public function destroy(Request $request) {
        Session::pull('cart.' .$request->product_id);
        return response(['message' => 'Produkt odebrán z košíku']);
    }

    public function update(Request $request) {
        if($request->count) {
            $attributes = Session::get('cart.' . $request->product . '.0.values');
            $product = Product::find($request->product);
            foreach($attributes as $value) {
                foreach($product->values as $productValue) {
                    if($productValue->id == $value) {
                        if($productValue->pivot->stock < $request->count) {
                            Session::put('cart.' . $request->product . '.0.count',(int)$productValue->pivot->stock);
                            return response(['warning' => 'Snažíte se objednat více než máme na skladě']);
                        }
                    }
                }
            }
            Session::put('cart.' . $request->product . '.0.count',(int)$request->count);

            $products = Product::whereIn('id', array_keys(Session::get('cart')))->get();
            $total = (new CartService())->getCartTotal(Session::get('cart'), $products, Session::get('checkout'));
            Session::put('total', $total);

            return response(['success' => 'Upraveno množství']);
        } else {
            Session::put('cart.' . $request->product . '.0.values.' . $request->attribute,(int)$request->value);
            return response(['success' => 'Upravena možnost']);
        }
    }

    public function showProducts() {
        $session = Session::get('cart');
        if(!isset($session)) {
            return response(['message' => 'Košík je prázdný.']);
        }
        $products = Product::whereIn('id', array_keys($session))->get();
        return ProductsResource::collection($products);
    }
    
    public function getCartSession() {
        return response(Session::get('cart'));
    }

    public function getCheckoutSession() {
        return response(Session::get('checkout'));
    }

    public function getTotalSession() {
        return response(Session::get('total'));
    }
}
