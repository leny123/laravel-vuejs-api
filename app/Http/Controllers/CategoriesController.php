<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use App\Http\Requests\NameSlugRequest;
use App\Http\Resources\CategoriesResource;
use App\Models\Category;

class CategoriesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return CategoriesResource::collection(Category::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NameSlugRequest $request)
    {
        if(Auth::check()) {
            if(!$request->user()->hasRole('admin')) {
                return response(['error' => 'Nemáte dostatečná oprávnění']);
            }
        } else {
            return response(['error' => 'Nemáte dostatečná oprávnění']);
        }
        $category = Category::create($request->validated());
        return response(['success' => 'Kategorie úspěšně vytvořena', 'category' => $category->id]);
    }

    public function storeCategoryImage(Request $request) {
        if(Auth::check()) {
            if(!$request->user()->hasRole('admin')) {
                return response(['error' => 'Nemáte dostatečná oprávnění']);
            }
        } else {
            return response(['error' => 'Nemáte dostatečná oprávnění']);
        }
        $category = Category::find($request->category);

        $newImageName = time() . '-' . 'category_photo' . '.' . $request->image->extension();
        Storage::delete('public/images/categories/' . $category->image);
        Storage::put('public/images/categories/' . $newImageName, File::get($request->image));
        $category->image = $newImageName;
        $category->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        return new CategoriesResource($category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NameSlugRequest $request, Category $category)
    {
        if(Auth::check()) {
            if(!$request->user()->hasRole('admin')) {
                return response(['error' => 'Nemáte dostatečná oprávnění']);
            }
        } else {
            return response(['error' => 'Nemáte dostatečná oprávnění']);
        }
        $category->update($request->validated());
        return response(['success' => 'Kategorie úspěšně aktualizována','category' => $category->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Category $category)
    {
        if(Auth::check()) {
            if(!$request->user()->hasRole('admin')) {
                return response(['error' => 'Nemáte dostatečná oprávnění']);
            }
        } else {
            return response(['error' => 'Nemáte dostatečná oprávnění']);
        }
        $category->delete();
        return response(['success' => 'Kategorie úspěšně odebrána']);
    }

    public function showFeatured() {
        return CategoriesResource::collection(Category::where('featured', 1)->get());
    }

    public function updateFeatured(Request $request) {
        if(Auth::check()) {
            if(!$request->user()->hasRole('admin')) {
                return response(['error' => 'Nemáte dostatečná oprávnění']);
            }
        } else {
            return response(['error' => 'Nemáte dostatečná oprávnění']);
        }
        $category = Category::find($request->category);
        if($category->featured == 1) {
            $category->featured = 0;
        } else {
            $category->featured = 1;
        }
        $category->save();
        return response(['success' => 'Úspěšně jste změnili nastavení kategorie']);
    }
}
