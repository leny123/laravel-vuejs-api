<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductValuesResource;
use App\Models\Value;
use App\Models\Product;

class ValueProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Product $product)
    {
        return ProductValuesResource::collection($product->values);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product, Value $value)
    {
        foreach($product->values as $productValue) {
            if($productValue->id == $value->id) {
                return new ProductValuesResource($productValue);
            }
        }
        abort(404);
    }
}
