<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Resources\OrdersResource;
use App\Services\CartService;
use App\Models\Order;
use App\Models\Sold;
use App\Models\Product;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(Auth::check()) {
            if(!$request->user()->hasRole('admin')) {
                return response(['error' => 'Nemáte dostatečná oprávnění']);
            }
        } else {
            return response(['error' => 'Nemáte dostatečná oprávnění']);
        }
        return OrdersResource::collection(Order::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cartSession = Session::get('cart');
        $checkoutSession = Session::get('checkout');
        $cartProducts = Product::find(array_keys($cartSession));
        if((new CartService())->checkOrderStock($cartSession, $cartProducts)) {
            return response(['error' => 'Snažíte se objednat více kusů než máme na skladě']);
        }
        $order = Order::create([
            'status' => 0,
            'total' => (new CartService())->getCartTotal($cartSession, $cartProducts, $checkoutSession),
            'email' => $checkoutSession['email'],
            'first_name' => $checkoutSession['first_name'],
            'last_name' => $checkoutSession['last_name'],
            'city' => $checkoutSession['city'],
            'street' => $checkoutSession['street'],
            'zipcode' => $checkoutSession['zipcode'],
            'phone' => $checkoutSession['phone'],
            'delivery' => $checkoutSession['delivery'],
            'payment' => $checkoutSession['payment'],
        ]);

        foreach($cartProducts as $product) {
            foreach($product->values as $value) {
                $product->values()->detach($value);
                $product->values()->attach([
                    $value->id => [
                        'stock' => $value->pivot->stock -= $cartSession[$product->id][0]['count']
                    ]
                ]);
            }
        }
        foreach($cartSession as $key => $product) {
            $order->products()->attach([
                $key => [
                    'count' => $product[0]['count'],
                    'values' => serialize($product[0]['values'])
                ]
            ]);
            Sold::create([
                'product_id' => $key,
                'count' => $product[0]['count']
            ]);
        }
        if(Auth::check()) {
            $request->user()->products()->sync(array_keys($cartSession));
        }
        Session::forget('cart');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Order $order)
    {
        if(Auth::check()) {
            if(!$request->user()->hasRole('admin')) {
                return response(['error' => 'Nemáte dostatečná oprávnění']);
            }
        } else {
            return response(['error' => 'Nemáte dostatečná oprávnění']);
        }
        return new OrdersResource($order);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Order $order, Request $request)
    {
        if(Auth::check()) {
            if(!$request->user()->hasRole('admin')) {
                return response(['error' => 'Nemáte dostatečná oprávnění']);
            }
        } else {
            return response(['error' => 'Nemáte dostatečná oprávnění']);
        }
        $order->status = 1;
        $order->save();

        return response()->json(['message' => 'Objednávka úspěšně vyřízena']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
