<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Resources\SubcategoriesResource;
use App\Http\Requests\NameSlugRequest;
use App\Models\Subcategory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

class SubcategoriesController extends Controller
{

    public function index()
    {
        return SubcategoriesResource::collection(Subcategory::all());
    }

    public function store(NameSlugRequest $request)
    {
        if(Auth::check()) {
            if(!$request->user()->hasRole('admin')) {
                return response(['error' => 'Nemáte dostatečná oprávnění']);
            }
        } else {
            return response(['error' => 'Nemáte dostatečná oprávnění']);
        }
        $subcategory = Subcategory::create($request->validated());
        $subcategory->categories()->attach($request->category);
        return response(['success' => 'Podkategorie úspěšně vytvořena']);
    }

    public function show(Subcategory $subcategory)
    {
        return new SubcategoriesResource($subcategory);
    }

    public function update(NameSlugRequest $request, Subcategory $subcategory)
    {
        if(Auth::check()) {
            if(!$request->user()->hasRole('admin')) {
                return response(['error' => 'Nemáte dostatečná oprávnění']);
            }
        } else {
            return response(['error' => 'Nemáte dostatečná oprávnění']);
        }
        $subcategory->update($request->validated());
        return response(['success' => 'Podkategorie úspěšně aktualizována']);
    }

    public function destroy(Request $request, Subcategory $subcategory)
    {
        if(Auth::check()) {
            if(!$request->user()->hasRole('admin')) {
                return response(['error' => 'Nemáte dostatečná oprávnění']);
            }
        } else {
            return response(['error' => 'Nemáte dostatečná oprávnění']);
        }
        $subcategory->delete();
        return response(['success' => 'Podkategorie úspěšně odebrána']);
    }
}
