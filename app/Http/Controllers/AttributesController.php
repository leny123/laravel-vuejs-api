<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Requests\NameSlugRequest;
use App\Http\Resources\AttributesResource;
use App\Models\Attribute;


class AttributesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return AttributesResource::collection(Attribute::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NameSlugRequest $request)
    {
        if(Auth::check()) {
            if(!$request->user()->hasRole('admin')) {
                return response(['error' => 'Nemáte dostatečná oprávnění']);
            }
        } else {
            return response(['error' => 'Nemáte dostatečná oprávnění']);
        }
        Attribute::create($request->validated());
        return response(['success' => 'Možnost úspěšně vytvořena']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Attribute $attribute)
    {
        return new AttributesResource($attribute);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NameSlugRequest $request, Attribute $attribute)
    {
        if(Auth::check()) {
            if(!$request->user()->hasRole('admin')) {
                return response(['error' => 'Nemáte dostatečná oprávnění']);
            }
        } else {
            return response(['error' => 'Nemáte dostatečná oprávnění']);
        }
        $attribute->update($request->validated());
        return response(['success' => 'Možnost úspěšně aktualizována.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Attribute $attribute)
    {
        $attribute->delete();
        return response(['success' => 'Možnost úspěšně odebrána.']);
    }
}
