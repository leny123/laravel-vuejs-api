<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductsListingResource;
use App\Models\Subcategory;

class ProductSubcategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Subcategory $subcategory)
    {
        return ProductsListingResource::collection(Subcategory::find($subcategory)->first()->products()->paginate(10));
    }
}
