<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use App\Services\ProductService;
use App\Http\Resources\ProductsResource;
use App\Http\Resources\ProductsListingResource;
use App\Http\Requests\ProductRequest;
use App\Models\Product;
use App\Models\Category;
use App\Models\Subcategory;
use Illuminate\Support\Facades\Session;

class ProductsController extends Controller
{
    public function showProductsListing() {
        return view('obchod.index', [
            'category' => 0,
            'subcategory' => 0,
            'subcategory_slug' => 0,
            'routeurl' => ''
        ]);
    }
    public function showCategory($category) {
        $categoryId = Category::where('slug', $category)->first()->id;

        return view('obchod.index', [
            'category' => $categoryId,
            'subcategory' => 0,
            'subcategory_slug' => 0,
            'routeurl' => 'products.category'
        ]);
    }
    public function showSubcategory($category, $subcategory) {
        $categoryId = Category::where('slug', $category)->first()->id;
        $subcategoryId = Subcategory::where('slug', $subcategory)->first()->id;
        
        return view('obchod.index', [
            'category' => $categoryId,
            'subcategory' => $subcategoryId,
            'subcategory_slug' => $subcategory,
            'routeurl' => 'products.subcategory'
        ]);
    }
    public function index() {
        return ProductsListingResource::collection(Product::paginate(10));
    }

    public function store(ProductRequest $request) {
        if(Auth::check()) {
            if(!$request->user()->hasRole('admin')) {
                return response(['error' => 'Nemáte dostatečná oprávnění']);
            }
        } else {
            return response(['error' => 'Nemáte dostatečná oprávnění']);
        }
        $request->validated();

        if($request->sale > 0) {
            $price = $request->sale;
            $last_price = $request->price;
        } else {
            $price = $request->price;
            $last_price = 0;
        }
        $product = Product::create([
            'name' => $request->name,
            'description' => $request->description,
            'image' => '',
            'price' => $price,
            'last_price' => $last_price
        ]);

        $product->categories()->attach($request->category);
        $product->subcategories()->attach($request->subcategory);
        foreach($request->stock as $stock) {
            $product->values()->attach([
                $stock['value'] => [
                    'stock' => $stock['stock']
                ]
            ]);
        }
        foreach($request->attribute_ids as $attribute) {
            $product->attributes()->attach($attribute);
        }

        return response(['success' => 'Úspěšně jste vytvořili produkt','productId' => $product->id]);
    }

    public function storeProductImage(Request $request) {
        if(Auth::check()) {
            if(!$request->user()->hasRole('admin')) {
                return response(['error' => 'Nemáte dostatečná oprávnění']);
            }
        } else {
            return response(['error' => 'Nemáte dostatečná oprávnění']);
        }
        $product = Product::find($request->product);

        $newImageName = time() . '-' . 'product_photo' . '.' . $request->image->extension();
        Storage::delete('public/images/products/' . $product->image);
        Storage::put('public/images/products/' . $newImageName, File::get($request->image));
        $product->image = $newImageName;
        $product->save();
    }

    public function update(ProductRequest $request, Product $product) {
        if(Auth::check()) {
            if(!$request->user()->hasRole('admin')) {
                return response(['error' => 'Nemáte dostatečná oprávnění']);
            }
        } else {
            return response(['error' => 'Nemáte dostatečná oprávnění']);
        }
        $request->validated();

        if($request->sale > 0) {
            $price = $request->sale;
            $last_price = $request->price;
        } else {
            $price = $request->price;
            $last_price = 0;
        }
        $product->name = $request->name;
        $product->description = $request->description;
        $product->price = $price;
        $product->last_price = $last_price;
        $product->save();

        $product->categories()->detach();
        $product->subcategories()->detach();

        $product->categories()->attach($request->category);
        $product->subcategories()->attach($request->subcategory);
        $product->values()->detach();
        foreach($request->stock as $stock) {
            $product->values()->attach([
                $stock['value'] => [
                    'stock' => $stock['stock']
                ]
            ]);
        }
        $product->attributes()->detach();
        foreach($request->attribute_ids as $attribute) {
            $product->attributes()->attach($attribute);
        }

        return response(['success' => 'Úspěšně jste upravili produkt']);
    }

    public function destroy(Request $request, Product $product) {
        if(Auth::check()) {
            if(!$request->user()->hasRole('admin')) {
                return response(['error' => 'Nemáte dostatečná oprávnění']);
            }
        } else {
            return response(['error' => 'Nemáte dostatečná oprávnění']);
        }
        $product->delete();

        return response(['success' => 'Úspěšně jste odebrali produkt']);
    }

    public function show(Product $product) {
        return new ProductsResource($product);
    }

    public function categoryProducts($slug) {
        $category = Category::where('slug', $slug)->first();

        if(isset($category->products)) {
            return ProductsResource::collection($category->products);
        } else {
            return 'Kategorie nenalezena.';
        }
    }

    public function subcategoryProducts($slug) {
        $subcategory = Subcategory::where('slug', $slug)->first();

        if(isset($subcategory->products)) {
            return ProductsResource::collection($subcategory->products);
        } else {
            return 'Podkategorie nenalezena.';
        }
    }
    
    public function showProducts() {
        return view('admin.products.index');
    }

    public function filterProducts(Request $request) {
        $products = (new ProductService())->getFilteredProducts($request);
        return ProductsListingResource::collection($products);
    }

    public function searchProducts(Request $request) {
        $products = Product::where('name', 'like', "%$request->productName%")->get()->take(20);
        if(count($products)) {
            return response(['data' => ProductsResource::collection($products)]);
        } else {
            return response(['message' => 'Produkt nenalezen.']);
        }
        
    }

    public function showFeatured() {
        $products = Product::orderBy('price', 'desc')->get()->take(5);
        return ProductsListingResource::collection($products);
    }
}
