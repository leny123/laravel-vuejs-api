<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function checkPermission(Request $request)
    {
        if(Auth::check()) {
            ($request->user()->hasRole('Admin'))
            ? $show = 1
            : $show = 0;
            
            $permissions = [
                'show' => $show
            ];
        } else {
            $permissions = [
                'show' => 0
            ];
        }
        return response($permissions);
    }
}
