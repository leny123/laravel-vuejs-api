<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Resources\RatingsResource;
use App\Http\Requests\RatingRequest;
use App\Models\Product;
use App\Models\Rating;

class RatingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Product $product)
    {
        return RatingsResource::collection($product->ratings);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RatingRequest $request, Product $product)
    {
        if(Auth::check()) {
            foreach($request->user()->products as $userProduct) {
                if($userProduct->id == $product->id) {
                    if(count($product->ratings) == 0) {
                        $rating = Rating::create($request->validated());
                        $product->ratings()->attach($rating);
                        $request->user()->ratings()->attach($rating);
                        return response(['success' => 'Přidána recenze']);
                    }
                    foreach($product->ratings as $rating) {
                        foreach($rating->users as $userRating) {
                            if($userRating->pivot->user_id != $request->user()->id) {
                                $rating = Rating::create($request->validated());
                                $product->ratings()->attach($rating);
                                $request->user()->ratings()->attach($rating);
                                return response(['success' => 'Přidána recenze']);
                            }
                        }
                        return response(['warning' => 'Na tenhle produkt jste již přidali recenzi']);
                    }
                }
            }
            return response(['error' => 'Nemůžete dělat recenzi na produkt, který jste si nezakoupili']);
        } else {
            return response(['warning' => 'Pro přidání recenze se musíte přihlásit']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Product $product, Rating $rating)
    {
        if(!$rating->users()->first()->pivot->user_id == $request->user()->id) {
            return response(['error' => 'Tuhle recenzi nevlastníte']);
        }
        $rating->users()->detach();
        $rating->products()->detach();
        $rating->delete();
        return response(['success' => 'Recenze odebrána']);
    }

    public function canDestroy(Request $request,) {
        $product = Product::find($request->product);
        foreach($product->ratings as $rating) {
            if($rating->users()->first()->pivot->user_id == $request->user()->id) {
                return $rating->id;
            }
        }
        return 0;
    }
}
