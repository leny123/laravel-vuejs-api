<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductsListingResource;
use App\Models\Category;

class ProductCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Category $category)
    {
        return ProductsListingResource::collection(Category::find($category->id)->products()->paginate(10));
    }
}
