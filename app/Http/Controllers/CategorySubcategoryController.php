<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\SubcategoriesResource;
use App\Http\Requests\NameSlugRequest;
use App\Models\Subcategory;
use App\Models\Category;

class CategorySubcategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Category $category)
    {
        return SubcategoriesResource::collection($category->subcategories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NameSlugRequest $request, Category $category)
    {
        if(Auth::check()) {
            if(!$request->user()->hasRole('admin')) {
                return response(['error' => 'Nemáte dostatečná oprávnění']);
            }
        } else {
            return response(['error' => 'Nemáte dostatečná oprávnění']);
        }
        $category->subcategories()->create($request->validated());
        return response(['success' => 'Úspěšně jste vytvořili podkategorii']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category, Subcategory $subcategory)
    {
        if($category->id != $subcategory->categories->first()->pivot->category_id) {
            abort(404);
        }
        return new SubcategoriesResource($subcategory);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NameSlugRequest $request, Category $category, Subcategory $subcategory)
    {
        if(Auth::check()) {
            if(!$request->user()->hasRole('admin')) {
                return response(['error' => 'Nemáte dostatečná oprávnění']);
            }
        } else {
            return response(['error' => 'Nemáte dostatečná oprávnění']);
        }
        if($category->id != $subcategory->categories->first()->pivot->category_id) {
            abort(404);
        }
        $subcategory->update($request->validated());
        return response(['success' => 'Úspěšně jste aktualizovali podkategorii']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Category $category, Subcategory $subcategory)
    {
        if(Auth::check()) {
            if(!$request->user()->hasRole('admin')) {
                return response(['error' => 'Nemáte dostatečná oprávnění']);
            }
        } else {
            return response(['error' => 'Nemáte dostatečná oprávnění']);
        }
        if($category->id != $subcategory->categories->first()->pivot->category_id) {
            abort(404);
        }
        $subcategory->categories()->detach();
        $subcategory->delete();
        return response(['success' => 'Úspěšně jste odstranili podkategorii']);
    }
}
