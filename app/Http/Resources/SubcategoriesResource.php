<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SubcategoriesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (string)$this->id,
                'type' => 'subcategories',
                'attributes' => [
                    'display_name' => $this->display_name,
                    'slug' => $this->slug,
                    'created_at' => $this->created_at,
                    'updated_at' => $this->updated_at,
                ]
        ];
    }
}
