<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\ProductValuesResource;
use Illuminate\Support\Facades\Auth;
use App\Models\Sold;

class ProductsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        
        return [
            'id' => (string)$this->id,
                'type' => 'products',
                'attributes' => [
                    'name' => $this->name,
                    'description' => $this->description,
                    'image' => url('storage') . '/images/products/' . $this->image,
                    'price' => $this->price,
                    'last_price' => $this->last_price,
                    'rating' => $this->getRating(),
                    'solds' => /* $this->when($this->checkAdmin($request->user()), $this->getSolds()) */ $this->getSolds(),
                    'created_at' => $this->created_at,
                    'updated_at' => $this->updated_at,
                    'category' => $this->categories,
                    'subcategory' => $this->subcategories,
                    'attribute_values' => $this->getProductAttributes(),
                ]
        ];
    }

    public function getProductAttributes() {
        $array = [];
        foreach($this->attributes as $attribute) {
            $attributeValues = [];
            foreach($this->values as $value) {
                if($value->attributes->first()->id == $attribute->id) {
                    array_push($attributeValues, new ProductValuesResource($value));
                }
            }
            $attributeResource = [
                'id' => $attribute->id,
                'display_name' => $attribute->display_name,
                'slug' => $attribute->slug,
                'values' => $attributeValues
            ];
            array_push($array,$attributeResource);
        }
        return $array;
    }

    public function getRating() {
        $allRatings = 0;
        foreach($this->ratings as $rating) {
            $allRatings += $rating->stars;
        }
        if($allRatings != 0) {
            $allRatings = $allRatings / count($this->ratings);
        }
        
        return $allRatings;
    }

    public function getSolds() {
        return Sold::where('product_id', $this->id)->get();
    }

    public function checkAdmin($user) {
        if(Auth::check()) {
            if($user->hasRole('Admin')) {
                return true;
            }
            return false;
        }
        return false;
    }
}
