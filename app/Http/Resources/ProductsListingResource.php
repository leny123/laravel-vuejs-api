<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductsListingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (string)$this->id,
                'type' => 'products',
                'attributes' => [
                    'name' => $this->name,
                    'description' => $this->description,
                    'image' => url('storage/images/products') . '/' . $this->image,
                    'price' => $this->price,
                    'last_price' => $this->last_price,
                    'rating' => $this->getRating(),
                    'created_at' => $this->created_at,
                    'updated_at' => $this->updated_at,
                ]
        ];
    }
    public function getRating() {
        $allRatings = 0;
        foreach($this->ratings as $rating) {
            $allRatings += $rating->stars;
        }
        if($allRatings != 0) {
            $allRatings = $allRatings / count($this->ratings);
        }
        
        return $allRatings;
    }
}
