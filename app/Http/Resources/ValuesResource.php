<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ValuesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (string)$this->id,
                'type' => 'values',
                'attributes' => [
                    'display_value' => $this->display_value,
                    'slug' => $this->slug,
                    'created_at' => $this->created_at,
                    'updated_at' => $this->updated_at,
                    'attribute' => $this->attributes->first()->id
                ]
        ];
    }
}
