<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoriesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (string)$this->id,
            'type' => 'categories',
            'attributes' => [
                'display_name' => $this->display_name,
                'slug' => $this->slug,
                'image' => url('storage/images/categories') . '/' . $this->image,
                'featured' => $this->featured,
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,
                'subcategories' => $this->subcategories
            ]
        ];
    }
}
