<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductsOrdersResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (string)$this->id,
                'type' => 'products',
                'attributes' => [
                    'name' => $this->name,
                    'description' => $this->description,
                    'image' => $this->image,
                    'price' => $this->price,
                    'last_price' => $this->last_price,
                    'created_at' => $this->created_at,
                    'updated_at' => $this->updated_at,
                ]
        ];
    }
}
