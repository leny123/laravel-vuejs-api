<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\ProductsOrdersResource;
use App\Http\Resources\ValuesOrderResource;
use App\Http\Resources\AttributesResource;
use Illuminate\Support\Facades\Auth;
use App\Models\Attribute;
use App\Models\Value;
use App\Models\Product;

class OrdersResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if(/* $this->checkPermission($request) */ true) {
            return [
                'id' => (string)$this->id,
                    'type' => 'orders',
                    'attributes' => [
                        'status' => $this->status,
                        'total' => $this->total,
                        'email' => $this->email,
                        'first_name' => $this->first_name,
                        'last_name' => $this->last_name,
                        'city' => $this->city,
                        'street' => $this->street,
                        'zipcode' => $this->zipcode,
                        'phone' => $this->phone,
                        'delivery' => $this->delivery,
                        'payment' => $this->payment,
                        'created_at' => date('d.m  h:i:s',strtotime($this->created_at)), 
                        'product_values' => $this->getProductAttributes(),
                    ]
            ];
        } else {
            return [
                'Nemáte dostatečná oprávnění.'
            ];
        }
        
    }
    public function getProductAttributes() {
        $valueArray = [];
        foreach($this->products as $product) {
            $serializedValues = [];
            foreach(unserialize($product->pivot->values) as $attribute => $value) {
                $currentAttribute = Attribute::find($attribute);
                $attributeResource = [
                    'id' => $currentAttribute->id,
                    'type' => 'attributes',
                    'attributes' => [
                        'display_name' => $currentAttribute->display_name,
                        'slug' => $currentAttribute->slug,
                        'created_at' => $currentAttribute->created_at,
                        'updated_at' => $currentAttribute->updated_at,
                        'value' => new ValuesResource(Value::find($value))
                    ]
                ];
                array_push($serializedValues, $attributeResource);
            }
            $productResource = [
                'id' => $product->id,
                'type' => 'products',
                'attributes' => [
                    'name' => $product->name,
                    'description' => $product->description,
                    'image' => $product->image,
                    'price' => $product->price,
                    'last_price' => $product->last_price,
                    'created_at' => $product->created_at,
                    'updated_at' => $product->updated_at,
                    'count' => $product->pivot->count,
                    'values' => $serializedValues,
                ]
            ];
            array_push($valueArray, $productResource);
        }
        return $valueArray;
    }
    public function checkPermission($request) {
        if(Auth::check()) {
            return $request->user()->hasRole('admin');
        } else {
            return false;
        }
    }
}
