<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RatingsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (string)$this->id,
                'type' => 'ratings',
                'attributes' => [
                    'stars' => $this->stars,
                    'text' => $this->text,
                    'user' => $this->users()->first()->name,
                    'created_at' => $this->created_at,
                    'updated_at' => $this->updated_at,
                ]
        ];
    }
}
