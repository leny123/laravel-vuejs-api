<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    use HasFactory;

    protected $fillable = [
        'display_name',
        'slug'
    ];

    public function products() {
        return $this->belongsToMany(Product::class, 'attribute_product_subattribute')
        ->withPivot('subattribute_id');
    }

    public function subattributes() {
        return $this->belongsToMany(Subattribute::class, 'attribute_product_subattribute')
        ->withPivot('product_id');
    }

    /* public function productValues() {
        return $this->belongsToMany(Value::class, 'attribute_product_subattribute_value');
        ->withPivot('product_id')
        ->withPivot('subattribute_id')
        ->withPivot('stock');
    } */

    public function values() {
        return $this->belongsToMany(Value::class);
    }

    
}
