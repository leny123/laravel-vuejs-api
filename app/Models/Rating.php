<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    use HasFactory;

    public $fillable = [
        'stars',
        'text',
    ];

    public function products() {
        return $this->belongsToMany(Product::class);
    }

    public function users() {
        return $this->belongsToMany(User::class);
    }
}
