<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Value extends Model
{
    use HasFactory;

    protected $fillable = [
        'display_value',
        'slug'
    ];

    public function products() {
        return $this->belongsToMany(Product::class)
        ->withPivot('stock');
    }

   /*  public function productSubattributes() {
        return $this->belongsToMany(Subattribute::class, 'attribute_product_subattribute')
        ->withPivot('stock');
    }

    public function productAttributes() {
        return $this->belongsToMany(Attribute::class, 'attribute_product_subattribute')
        ->withPivot('stock');
    } */

    public function attributes() {
        return $this->belongsToMany(Attribute::class);
    }
    public function subattributes() {
        return $this->belongsToMany(Subattribute::class);
    }
    
}
