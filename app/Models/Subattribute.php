<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subattribute extends Model
{
    use HasFactory;

    protected $fillable = [
        'display_name',
        'slug'
    ];
    
    public function products() {
        return $this->belongsToMany(Product::class, 'attribute_product_subattribute')
        ->withPivot('attribute_id');
    }

    public function attributes() {
        return $this->belongsToMany(Attribute::class, 'attribute_product_subattribute')
        ->withPivot('product_id');
    }

    /* public function productValues() {
        return $this->belongsToMany(Attribute::class, 'attribute_product_subattribute')
        ->withPivot('product_id')
        ->withPivot('attribute_id')
        ->withPivot('stock');
    } */

    public function values() {
        return $this->belongsToMany(Value::class);
    }
}
