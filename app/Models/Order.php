<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'status',
        'total',
        'email',
        'first_name',
        'last_name',
        'city',
        'street',
        'zipcode',
        'phone',
        'delivery',
        'payment',
    ];

    public function products() {
        return $this->belongsToMany(Product::class)
        ->withPivot('values')
        ->withPivot('count');
    }
}
