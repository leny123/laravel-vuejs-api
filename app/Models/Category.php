<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $fillable = [
        'display_name',
        'slug'
    ];

    public function subcategories() {
        return $this->belongsToMany(Subcategory::class);
    }

    public function products() {
        return $this->belongsToMany(Product::class);
    }
}
