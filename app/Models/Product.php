<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'image',
        'price',
        'last_price'
    ];

    public function categories() {
        return $this->belongsToMany(Category::class);
    }

    public function subcategories() {
        return $this->belongsToMany(Subcategory::class);
    }

    public function attributes() {
        return $this->belongsToMany(Attribute::class, 'attribute_product_subattribute')
        ->withPivot('subattribute_id');
    }

    public function subattributes() {
        return $this->belongsToMany(Subattribute::class, 'attribute_product_subattribute')
        ->withPivot('attribute_id');
    }

    public function values() {
        return $this->belongsToMany(Value::class)
        ->withPivot('stock');
    }

    public function orders() {
        return $this->belongsToMany(Order::class);
    }

    public function ratings() {
        return $this->belongsToMany(Rating::class);
    }

    public function users() {
        return $this->belongsToMany(User::class);
    }
}
