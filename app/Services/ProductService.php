<?php 

namespace App\Services;

use App\Http\Resources\ProductsResource;
use App\Models\Product;
use App\Models\Category;
use App\Models\Subcategory;

class ProductService {

    public function getFilteredProducts($request) {
        if($request->subcategory) {
            $subcategory = Subcategory::find($request->subcategory);
            if($subcategory->products) {
                if($request->filter['sale'] == 1) {
                    $products = $subcategory->products()->where('last_price', '>', 0)->get();
                }
                if($request->filter['cheapest'] == 1) {
                (isset($products))
                 ? $products = Product::whereIn('id', $products->pluck('id'))->orderBy('price', 'ASC')->get()
                 : $products = $subcategory->products()->orderBy('price', 'ASC')->get();
                }

                if($request->filter['expensive'] == 1) {
                (isset($products))
                 ? $products = Product::whereIn('id', $products->pluck('id'))->orderBy('price', 'DESC')->get()
                 : $products = $subcategory->products()->orderBy('price', 'DESC')->get();
                }
            }
        } elseif($request->category) {
            $category = Category::find($request->category);
            if($category->products) {
                if($request->filter['sale'] == 1) {
                    $products = $category->products()->where('last_price', '>', 0)->get();
                }
                if($request->filter['cheapest'] == 1) {
                (isset($products))
                 ? $products = Product::whereIn('id', $products->pluck('id'))->orderBy('price', 'ASC')->get()
                 : $products = $category->products()->orderBy('price', 'ASC')->get();
                }

                if($request->filter['expensive'] == 1) {
                (isset($products))
                 ? $products = Product::whereIn('id', $products->pluck('id'))->orderBy('price', 'DESC')->get()
                 : $products = $category->products()->orderBy('price', 'DESC')->get();
                }
            }
        } else {
            $products = Product::all();
            if($request->filter['sale'] == 1) {
                $products = Product::where('last_price', '>', 0)->get();
            }
            if($request->filter['cheapest'] == 1) {
            (isset($products))
             ? $products = Product::whereIn('id', $products->pluck('id'))->orderBy('price', 'ASC')->get()
             : $products = Product::all()->orderBy('price', 'ASC')->get();
            }

            if($request->filter['expensive'] == 1) {
            (isset($products))
             ? $products = Product::whereIn('id', $products->pluck('id'))->orderBy('price', 'DESC')->get()
             : $products = Product::all()->orderBy('price', 'DESC')->get();
            }
        }
        return $products;
    }
}