<?php 

namespace App\Services;

class CartService {
    
    public function addToCartWithValues($request, $values) {
        $valueArray = [];
        foreach($values as $value) {
            $valueArray[$value->attributes()->first()->id] = $value->id;
        }
        $cartItem = [
            'values' => $valueArray,
            'count' => $request->count,
        ];
        return $cartItem;
    }

    public function addToCartDefault($product) {
        $valueArray = [];
        
        foreach($product->attributes as $attribute) {
            foreach($product->values as $value) {
                foreach($attribute->values as $attributeValue) {
                    if($value->id == $attributeValue->id) {
                        $valueArray[$attribute->id] = $attributeValue->id;
                    }
                }
            }
        }
        $cartItem = [
            'values' => $valueArray,
            'count' => 1,
        ];
        return $cartItem;
    }
    
    public function addToCheckout($request) {

        $checkoutArray = [
            'email' => $request->email,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'city' => $request->city,
            'street' => $request->street,
            'zipcode' => (int)$request->zipcode,
            'phone' => (int)$request->phone,
            'delivery' => $request->delivery,
            'payment' => $request->payment,
        ];
        return $checkoutArray;
    }

    public function checkStock($request, $product) {
        foreach($product->values as $productValue) {
            foreach($request->values as $value) {
                if($productValue->id == $value) {
                    if($productValue->pivot->stock == 0) {
                        return 0;
                    }
                    if($productValue->pivot->stock < $request->count) {
                        return 1;
                    }
                }
            }
        }
        return 3;
    }
    public function checkOrderStock($cartSession, $cartProducts) {
        foreach($cartProducts as $product) {
            foreach($product->values as $value) {
                if($value->pivot->stock < $cartSession[$product->id][0]['count']) {
                    return true;
                }
            }
        }
        return false;
    }
    public function getCartTotal($cartSession, $products, $checkoutSession = null) {
        $total = 0;
        foreach($products as $product) {
            $total += $cartSession[$product->id][0]['count'] * $product->price;
        }
        if($checkoutSession) {
            if($checkoutSession['payment'] == 'dobirka') {
                $total += 19;
            }
            if($checkoutSession['delivery'] == 'ppl') {
                $total += 79;
            }
            if($checkoutSession['delivery'] == 'ceska_posta') {
                $total += 59;
            }
        }
        
        return $total;
    }
}